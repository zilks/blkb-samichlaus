const Discord = require("discord.js");
const config = require("./config.json");

const client = new Discord.Client();

const prefix = "!";

client.on("ready", () => {
   console.log("Ich, dr Samichlaus, bi barat zum Gschänkli verteile!");
   const channel = client.channels.cache.get("767681798401949709");
   if (!channel) return;
   //channel.send("Ho ho ho!");
});

client.on("message", function(message) {
    if(message.author.bot) return;
    if(!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length);
    const args = commandBody.split(" ");
    const command = args.shift().toLowerCase();

    if(command === "hoisamichlaus") {
        message.reply(`Ho ho ho!`);
    }
});

client.on("guildMemberAdd", member => {
   const channel = member.guild.channels.cache.find(channel => channel.name === "general");
   if(!channel) return;
   channel.send(`Dr Samichlaus grüesst di, ${member}! Leider weiss i noni öb du Gschänkli verdient hesch ;)`);
});

client.login(config.BOT_TOKEN);